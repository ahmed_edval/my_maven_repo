Download jar http://www.smartclient.com/builds/SmartGWT/4.1d/LGPL/2014-02-16

1- Add jar to jars and cd to root and use .py file with 4 arguments to generate the command in mvn to use it like in:
	full_path_to_jars_dir 
	group 
	artifact 
	version

2- Run the command generated in mvn_commands file	

3- cd to repository and use the indexer jar like:
java -jar nexus-indexer-3.0.4-cli.jar -r . -i .

https://bitbucket.org/neil_rubens/rapidminer_maven-repo/wiki/Home